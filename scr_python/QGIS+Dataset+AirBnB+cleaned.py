
# coding: utf-8

# In[219]:


import pandas as pd #pour le data processing
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')
import numpy as np #pour l'algèbre linéaire
import scipy.stats as stats
import seaborn as sns
import csv

df = pd.read_csv("C:/Users/Admin/Documents/MEDAS/SIG/PROJET/listings_airbnb.csv", sep=",")


# In[220]:


df.shape


# In[221]:


df.head()


# In[222]:


variable= ['name', 'neighbourhood_group', 'last_review']
df.drop(variable, inplace=True, axis=1)
df 


# In[223]:


#calculer la moyenne d'une variable
price = df.price

def get_size(maliste):
    taille_l=0
    for i in maliste:
        taille_l+=1 #taille_liste = taille_liste + 1 => //+=
    return taille_l

def moyenne_liste(maliste):
    taille_liste = get_size(maliste)  
    
    somme=0
    for valeur in maliste:
        if not(np.isnan(valeur)):
            somme+=valeur
    
    moyenne=somme/taille_liste
    return moyenne

    
price_moyenne = moyenne_liste(price)
print(price_moyenne)


#commentaires_moyenne = moyenne_liste(df.calculated_host_listings_count)
#print(commentaires_moyenne)


# In[224]:


df.isnull().sum(axis=0)

#def replace_none_by_0(maliste):
 #   maliste = list(maliste)
  #  taille = get_size(maliste)
   # for i in range(taille):
    #    if np.isnan(maliste[i]):
     #       maliste[i] = 0
    #return maliste



# In[225]:


def replace_by_mean(maliste):
    taille= get_size(maliste)
    maliste = list(maliste)
    moyn = moyenne_liste(maliste)
    for i in range(taille):
        valeur = maliste[i]
        if np.isnan(maliste[i]) or (maliste[i] == 0):
            maliste[i] = moyn
    return maliste
            

reviews = df.reviews_per_month
reviews = replace_by_mean(reviews)
df.reviews_per_month = reviews


# In[226]:


df.isnull().sum(axis=0)


# In[227]:


df.dtypes


# In[228]:


def replace_none_by_user(maliste):
    maliste = list(maliste)
    taille = get_size(maliste)

    for i in range(taille):
        if isinstance(maliste[i], float):
            maliste[i] = "user"+str(i)
            print("user"+str(i))    
    return maliste

host_name = replace_none_by_user(df.host_name)
df.host_name = host_name
df.isnull().sum(axis=0)


# In[229]:


host_name.index("user55739")


# In[230]:


def test_zero_exist(maliste):
    
    for i in maliste:
        if i== 0:
            print("yes zero exists")
            return 0
    print("zero does not exist")
    return 0
            
test_zero_exist(df.price)


# In[231]:


price = df.price
price = replace_by_mean(price)
df.price = price


# In[232]:


test_zero_exist(df.price)


# In[253]:


df.iloc[48288,2] = "Smiley"


# In[254]:


df.iloc[48288,2] 


# In[259]:


df.to_csv(r"C:\Users\Admin\Documents\MEDAS\SIG\PROJET\listing_airbnb.csv", sep='\t')

